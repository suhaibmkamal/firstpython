def hello(x, y):
    print(x + y)


def helloReturn(x, y):
    return x + y


age = helloReturn(25, 25)
hello(10, 10)

print(age)
